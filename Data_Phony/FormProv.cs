﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data_Phony
{
    public partial class FormProv : Form
    {
        private int RowId;
        private String Request;

        public FormProv(DataGridViewRow Row)
        {
            InitializeComponent();
            this.Hide();

            if (Row == null)
            {
                this.Text = "Create new provider record";
                RowId = 0;
            }
            else
            {
                this.Text = "Modify provider record";
                RowId = (int)(Row.Cells["Id"].Value);

                TB_PE_Name.Text = Row.Cells["Name"].Value.ToString();
                TB_PE_Score.Text = (Row.Cells["Score"].Value.ToString()).Replace(',', '.');
            }
        }

        private void Bn_OK_Click(object sender, EventArgs e)
        {
            if (RowId == 0)
            {

                // Create new provider:
                Request = @"INSERT INTO Nikitin_ProviderTable (Name, Score)
                    VALUES ('" + TB_PE_Name.Text + "', CAST('" + TB_PE_Score.Text + "' AS float))";
            }
            else
            {
                // Modify contact:
                Request = @"UPDATE Nikitin_ProviderTable 
                    SET Name = '" + TB_PE_Name.Text + "', Score = CAST('" + TB_PE_Score.Text + @"' AS float)
                    WHERE (Id = " + RowId + ")";
            }

            using (SqlConnection Conn = new SqlConnection(Form1.ConnectionString))
            {
                SqlCommand Doit = new SqlCommand(Request, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
            }

            this.Close();
        }

        private void Bn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
