﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data_Phony
{
    public partial class FormCont : Form
    {
        private int RowId;
        private String Request;

        public FormCont(DataGridViewRow Row)
        {
            InitializeComponent();
            this.Hide();

            string Linkage = "SELECT Nikitin_ProviderTable.Id, Nikitin_ProviderTable.Name FROM Nikitin_ProviderTable";
            var Adapter = new SqlDataAdapter(Linkage, Form1.ConnectionString);
            var Providers = new DataTable();
            Adapter.Fill(Providers);
            DataGrid_Prov_Invisible.DataSource = Providers;

            LB_CE_Provider.Items.Clear();
            foreach (DataGridViewRow PRow in DataGrid_Prov_Invisible.Rows)
            {
                LB_CE_Provider.Items.Add(PRow.Cells["Name"].Value.ToString());
            }

            if (Row == null)
            {
                this.Text = "Create new contact record";
                Bn_LinkView.Enabled = false;
                RowId = 0;
            }
            else
            {
                this.Text = "Modify contact record";
                Bn_LinkView.Enabled = true;
                RowId = (int)(Row.Cells["Id"].Value);

                TB_CE_Number.Text = Row.Cells["Number"].Value.ToString();
                LB_CE_Type.SelectedIndex = LB_CE_Type.FindString(Row.Cells["Type"].Value.ToString());
                LB_CE_Provider.SelectedIndex = LB_CE_Provider.FindString(Row.Cells["Provider"].Value.ToString());
            }
        }

        private void Bn_CE_NewProv_Click(object sender, EventArgs e)
        {
            // Create new provider record:
            FormProv FormNewProv = new FormProv(null);
            if (FormNewProv.ShowDialog(this) == DialogResult.OK)
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
            FormNewProv.Dispose();

            string Linkage = "SELECT Nikitin_ProviderTable.Id, Nikitin_ProviderTable.Name FROM Nikitin_ProviderTable";
            var Adapter = new SqlDataAdapter(Linkage, Form1.ConnectionString);
            var Providers = new DataTable();
            Adapter.Fill(Providers);
            DataGrid_Prov_Invisible.DataSource = Providers;

            LB_CE_Provider.Items.Clear();
            foreach (DataGridViewRow PRow in DataGrid_Prov_Invisible.Rows)
            {
                LB_CE_Provider.Items.Add(PRow.Cells["Name"].Value.ToString());
            }
        }
        
        private void Bn_LinkView_Click(object sender, EventArgs e)
        {
            // View all abonents related to this contact:
            string Linkage = @"SELECT Nikitin_CARelationTable.AbonentId, 
                (ISNULL(Nikitin_AbonentTable.FirstName, '') + ' ' + ISNULL(Nikitin_AbonentTable.SecName, '') + ' ' + ISNULL(Nikitin_AbonentTable.SurName, '')) AS Name 
                FROM (Nikitin_CARelationTable RIGHT JOIN Nikitin_AbonentTable ON (Nikitin_CARelationTable.AbonentId = Nikitin_AbonentTable.Id)) 
                WHERE (Nikitin_CARelationTable.ContactId = " + RowId + ")";
            var Adapter = new SqlDataAdapter(Linkage, Form1.ConnectionString);
            var LinkTable = new DataTable();
            Adapter.Fill(LinkTable);

            DataGrid_ContLink.Columns.Add("ContactId", "ContactId");
            DataGrid_ContLink.Columns.Add("Number", "Number");
            foreach (DataRow Row in LinkTable.Rows)
            {
                DataGrid_ContLink.Rows.Add(Row.ItemArray);
            }
            DataGrid_ContLink.Columns["ContactId"].Visible = false;
        }

        private void Bn_LinkNew_Click(object sender, EventArgs e)
        {
            // Create new abonent record:
            FormAbon FormNewAbon = new FormAbon(null);
            if (FormNewAbon.ShowDialog(this) == DialogResult.OK)
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
            FormNewAbon.Dispose();
        }

        private void Bn_LinkExist_Click(object sender, EventArgs e)
        {
            // Link existing abonent to this contact:
            string Linkage = @"SELECT Nikitin_AbonentTable.Id AS AbonentId, 
                (ISNULL(Nikitin_AbonentTable.FirstName, '') + ' ' + ISNULL(Nikitin_AbonentTable.SecName, '') + ' ' + ISNULL(Nikitin_AbonentTable.SurName, '')) AS Name
                FROM Nikitin_AbonentTable
                WHERE ((Nikitin_AbonentTable.FirstName LIKE '" + TB_LinkName.Text + @"') OR
                 (Nikitin_AbonentTable.SecName LIKE '" + TB_LinkName.Text + @"') OR
			     (Nikitin_AbonentTable.FirstName + ' ' + Nikitin_AbonentTable.SurName LIKE '" + TB_LinkName.Text + @"') OR
			     (Nikitin_AbonentTable.FirstName + ' ' + Nikitin_AbonentTable.SecName LIKE '" + TB_LinkName.Text + @"') OR
			     (Nikitin_AbonentTable.SurName + ' ' + Nikitin_AbonentTable.FirstName LIKE '" + TB_LinkName.Text + @"') OR
                 (Nikitin_AbonentTable.SurName + ' ' + Nikitin_AbonentTable.FirstName + ' ' + Nikitin_AbonentTable.SecName LIKE '" + TB_LinkName.Text + @"'))";
            var Adapter = new SqlDataAdapter(Linkage, Form1.ConnectionString);
            var LinkTable = new DataTable();
            Adapter.Fill(LinkTable);
            if (DataGrid_ContLink.ColumnCount == 0)
            {
                DataGrid_ContLink.Columns.Add("AbonentId", "AbonentId");
                DataGrid_ContLink.Columns.Add("Name", "Name");
            }
            if (LinkTable.Rows.Count > 0)
            {
                DataGrid_ContLink.Rows.Add(LinkTable.Rows[0].ItemArray);
            }
            DataGrid_ContLink.Columns["ContactId"].Visible = false;
        }

        private void Bn_LinkDelete_Click(object sender, EventArgs e)
        {
            // Unlink selected abonent from this contact:
            DataGrid_ContLink.Rows.RemoveAt(DataGrid_ContLink.SelectedCells[0].RowIndex);
        }

        private void Bn_OK_Click(object sender, EventArgs e)
        {
            // Find provider's ID:
            int ProviderId = 0;
            foreach (DataGridViewRow PRow in DataGrid_Prov_Invisible.Rows)
            {
                if (LB_CE_Provider.Text.Contains(PRow.Cells["Name"].Value.ToString()))
                {
                    ProviderId = (int)(PRow.Cells["Id"].Value);
                }
            }

            if (RowId == 0)
            {

                // Create new contact:
                Request = @"INSERT INTO Nikitin_ContactTable (Number, Type, ProviderId)
                    VALUES ('" + TB_CE_Number.Text + "', '" + LB_CE_Type.Text + "', " + ProviderId + ")";
            }
            else
            {
                // Modify contact:
                Request = @"UPDATE Nikitin_ContactTable 
                    SET Number = '" + TB_CE_Number.Text + "', Type = '" + LB_CE_Type.Text + "', ProviderId = " + ProviderId + @"
                    WHERE (Id = " + RowId + ")";
            }

            using (SqlConnection Conn = new SqlConnection(Form1.ConnectionString))
            {
                SqlCommand Doit = new SqlCommand(Request, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
            }

            Request = @"SELECT Nikitin_ContactTable.Id FROM Nikitin_ContactTable
                WHERE (Number LIKE '" + TB_CE_Number.Text + "')";
            var Adapter = new SqlDataAdapter(Request, Form1.ConnectionString);
            var Table = new DataTable();
            Adapter.Fill(Table);
            RowId = (int)(Table.Rows[0].ItemArray[0]);

            // Update relation table:
            if (DataGrid_ContLink.Rows.Count > 0)
            {
                Request = "DELETE FROM Nikitin_CARelationTable WHERE (Nikitin_CARelationTable.ContactId = " + RowId + @")
                INSERT INTO Nikitin_CARelationTable (ContactId, AbonentId) VALUES";
                for (int i = 0; i < DataGrid_ContLink.Rows.Count; i++)
                {
                    if (i > 0) { Request += ","; }
                    Request += " (" + RowId + ", " + DataGrid_ContLink.Rows[i].Cells["ContactId"].Value.ToString() + ")";
                }
            }

            using (SqlConnection Conn = new SqlConnection(Form1.ConnectionString))
            {
                SqlCommand Doit = new SqlCommand(Request, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
            }

            this.Close();
        }

        private void Bn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
