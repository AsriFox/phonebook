﻿namespace Data_Phony
{
    partial class FormProv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Bn_Cancel = new System.Windows.Forms.Button();
            this.Bn_OK = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TB_PE_Score = new System.Windows.Forms.TextBox();
            this.TB_PE_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Bn_Cancel
            // 
            this.Bn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Bn_Cancel.Location = new System.Drawing.Point(124, 3);
            this.Bn_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cancel.Name = "Bn_Cancel";
            this.Bn_Cancel.Size = new System.Drawing.Size(64, 20);
            this.Bn_Cancel.TabIndex = 0;
            this.Bn_Cancel.Text = "Cancel";
            this.Bn_Cancel.UseVisualStyleBackColor = true;
            this.Bn_Cancel.Click += new System.EventHandler(this.Bn_Cancel_Click);
            // 
            // Bn_OK
            // 
            this.Bn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Bn_OK.Location = new System.Drawing.Point(52, 3);
            this.Bn_OK.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_OK.Name = "Bn_OK";
            this.Bn_OK.Size = new System.Drawing.Size(64, 20);
            this.Bn_OK.TabIndex = 0;
            this.Bn_OK.Text = "Submit";
            this.Bn_OK.UseVisualStyleBackColor = true;
            this.Bn_OK.Click += new System.EventHandler(this.Bn_OK_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Bn_Cancel);
            this.splitContainer1.Panel2.Controls.Add(this.Bn_OK);
            this.splitContainer1.Panel2MinSize = 28;
            this.splitContainer1.Size = new System.Drawing.Size(192, 101);
            this.splitContainer1.SplitterDistance = 72;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.TB_PE_Score, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB_PE_Name, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(192, 72);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // TB_PE_Score
            // 
            this.TB_PE_Score.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_PE_Score.Location = new System.Drawing.Point(52, 32);
            this.TB_PE_Score.Margin = new System.Windows.Forms.Padding(4);
            this.TB_PE_Score.Name = "TB_PE_Score";
            this.TB_PE_Score.Size = new System.Drawing.Size(136, 20);
            this.TB_PE_Score.TabIndex = 6;
            // 
            // TB_PE_Name
            // 
            this.TB_PE_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_PE_Name.Location = new System.Drawing.Point(52, 4);
            this.TB_PE_Name.Margin = new System.Windows.Forms.Padding(4);
            this.TB_PE_Name.Name = "TB_PE_Name";
            this.TB_PE_Name.Size = new System.Drawing.Size(136, 20);
            this.TB_PE_Name.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Score:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FormProv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(192, 101);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(152, 113);
            this.Name = "FormProv";
            this.Text = "Provider";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Bn_Cancel;
        private System.Windows.Forms.Button Bn_OK;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_PE_Score;
        private System.Windows.Forms.TextBox TB_PE_Name;
    }
}