﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data_Phony
{
    public partial class Form1 : Form
    {
        // private const string ConnectionString = "Data Source = 192.168.9.5; Initial Catalog = KIS; Persist Security Info = True; User ID = sa; Password = sa";
        public const string ConnectionString = "Data Source=(LocalDB)\\v11.0; AttachDbFilename=A:\\Documents\\Code\\DataProcessing\\Data_Phony_2\\Data_Phony\\TestDB.mdf; Integrated Security=True";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Update_DataGrid_Abon();
            Update_DataGrid_Cont();
            Update_DataGrid_Prov();

            Check_FindAbon.Checked = false;
            TabCtrl_Tables.SelectedIndex = 0;
        }

        private void Update_DataGrid_Abon()
        {
            var Request = "SELECT * FROM Nikitin_AbonentTable";
            var Adapter = new SqlDataAdapter(Request, ConnectionString);

            var AbonTable = new DataTable();
            Adapter.Fill(AbonTable);
            DataGrid_Abon.DataSource = AbonTable;

            DataGrid_Abon.Columns["Id"].Visible = false;
            DataGrid_Abon.Columns["SurName"].HeaderText = "Surname";
            DataGrid_Abon.Columns["FirstName"].HeaderText = "Name";
            DataGrid_Abon.Columns["SecName"].HeaderText = "Second name";
            DataGrid_Abon.Columns["BirthDate"].HeaderText = "Birth date";
        }

        private void Update_DataGrid_Cont()
        {
            var Request = "SELECT Nikitin_ContactTable.Id, Nikitin_ContactTable.Number, Nikitin_ContactTable.Type, Nikitin_ProviderTable.Name AS Provider ";
            Request += "FROM (Nikitin_ContactTable LEFT JOIN Nikitin_ProviderTable ON Nikitin_ContactTable.ProviderId = Nikitin_ProviderTable.Id)";

            var Adapter = new SqlDataAdapter(Request, ConnectionString);
            var ContTable = new DataTable();
            Adapter.Fill(ContTable);
            DataGrid_Cont.DataSource = ContTable;
            DataGrid_Cont.Columns["Id"].Visible = false;
        }

        private void Update_DataGrid_Prov()
        {
            var Request = "SELECT * FROM Nikitin_ProviderTable";
            var Adapter = new SqlDataAdapter(Request, ConnectionString);
            var ProvTable = new DataTable();
            Adapter.Fill(ProvTable);
            DataGrid_Prov.DataSource = ProvTable;
            DataGrid_Prov.Columns["Id"].Visible = false;
        }

        private void Update_DataGrid_FindCont()
        {
            var Request = "SELECT Nikitin_AbonentTable.SurName AS Surname,  Nikitin_AbonentTable.FirstName AS 'First name', Nikitin_AbonentTable.SecName AS 'Second name', ";
            if (Check_Search_Birthdate.Checked) { Request += "Nikitin_AbonentTable.BirthDate AS 'Birth date', "; }
            if (Check_Search_Address.Checked) { Request += "Nikitin_AbonentTable.Address, "; }
            if (Check_Search_Commentary.Checked) { Request += "Nikitin_AbonentTable.Commentary, "; }
            Request += @"Nikitin_ContactTable.Number, Nikitin_ContactTable.Type,	Nikitin_ProviderTable.Name AS Provider
             FROM ((Nikitin_AbonentTable JOIN Nikitin_CARelationTable ON Nikitin_AbonentTable.Id = Nikitin_CARelationTable.AbonentId)
             JOIN (Nikitin_ContactTable LEFT JOIN Nikitin_ProviderTable ON Nikitin_ContactTable.ProviderId = Nikitin_ProviderTable.Id)
             ON Nikitin_ContactTable.Id = Nikitin_CARelationTable.ContactId)";

            bool Search = false;
            Search |= (TB_Number.Text.Length > 0);
            Search |= (TB_Name.Text.Length > 0);
            Search |= (TB_Birthdate.Text.Length > 0);
            Search |= (TB_Address.Text.Length > 0);
            Search |= (TB_Commentary.Text.Length > 0);

            if (Search)
            {
                Request += @"WHERE
                 (Nikitin_ContactTable.Number LIKE '%" + TB_Number.Text + @"%') AND
                 ((Nikitin_AbonentTable.FirstName LIKE '" + TB_Name.Text + @"%') OR
                 (Nikitin_AbonentTable.SecName LIKE '" + TB_Name.Text + @"%') OR
			     (Nikitin_AbonentTable.FirstName + ' ' + Nikitin_AbonentTable.SurName LIKE '" + TB_Name.Text + @"%') OR
			     (Nikitin_AbonentTable.FirstName + ' ' + Nikitin_AbonentTable.SecName LIKE '" + TB_Name.Text + @"%') OR
			     (Nikitin_AbonentTable.SurName + ' ' + Nikitin_AbonentTable.FirstName LIKE '" + TB_Name.Text + @"%') OR
                 (Nikitin_AbonentTable.SurName + ' ' + Nikitin_AbonentTable.FirstName + ' ' + Nikitin_AbonentTable.SecName LIKE '" + TB_Name.Text + @"%'))";
                if (Check_Search_Birthdate.Checked)
                {
                    Request += " AND (CONVERT(nvarchar, Nikitin_AbonentTable.BirthDate, 104) LIKE '%" + TB_Birthdate.Text + "%')";
                }
                if (Check_Search_Address.Checked)
                {
                    Request += " AND (Nikitin_AbonentTable.Address LIKE '" + TB_Address.Text + "%')";
                }
                if (Check_Search_Commentary.Checked)
                {
                    Request += " AND (Nikitin_AbonentTable.Commentary LIKE '" + TB_Commentary.Text + "%')";
                }
            }

            var Adapter = new SqlDataAdapter(Request, ConnectionString);
            var BookTable = new DataTable();
            Adapter.Fill(BookTable);
            DataGrid_FindCont.DataSource = BookTable;
        }

        private void Check_Search_Birthdate_CheckedChanged(object sender, EventArgs e)
        {
            TB_Birthdate.Visible = Check_Search_Birthdate.Checked;
        }

        private void Check_Search_Address_CheckedChanged(object sender, EventArgs e)
        {
            TB_Address.Visible = Check_Search_Address.Checked;
        }

        private void Check_Search_Commentary_CheckedChanged(object sender, EventArgs e)
        {
            TB_Commentary.Visible = Check_Search_Commentary.Checked;
        }

        private void Check_FindAbon_CheckedChanged(object sender, EventArgs e)
        {
            if (Check_FindAbon.Checked)
            {
                Update_DataGrid_FindCont();
            }
        }

        private void TB_Name_TextChanged(object sender, EventArgs e)
        {
            if (Check_FindAbon.Checked)
            {
                Update_DataGrid_FindCont();
            }
        }

        private void TB_Number_TextChanged(object sender, EventArgs e)
        {
            if (Check_FindAbon.Checked)
            {
                Update_DataGrid_FindCont();
            }
        }

        private void TB_Birthdate_TextChanged(object sender, EventArgs e)
        {
            if (Check_FindAbon.Checked)
            {
                Update_DataGrid_FindCont();
            }
        }

        private void TB_Address_TextChanged(object sender, EventArgs e)
        {
            if (Check_FindAbon.Checked)
            {
                Update_DataGrid_FindCont();
            }
        }

        private void TB_Commentary_TextChanged(object sender, EventArgs e)
        {
            if (Check_FindAbon.Checked)
            {
                Update_DataGrid_FindCont();
            }
        }

        private void Bn_Abon_Update_Click(object sender, EventArgs e)
        {
            Update_DataGrid_Abon();
        }

        private void Bn_Abon_New_Click(object sender, EventArgs e)
        {
            FormAbon FormNewAbon = new FormAbon(null);
            if (FormNewAbon.ShowDialog(this) == DialogResult.OK)
            {
                System.Media.SystemSounds.Asterisk.Play();

                Update_DataGrid_Abon();
            }
            FormNewAbon.Dispose();
        }

        private void Bn_Abon_Modify_Click(object sender, EventArgs e)
        {
            if (DataGrid_Abon.SelectedCells.Count > 0)
            {
                int RowIndex = DataGrid_Abon.SelectedCells[0].RowIndex;
                foreach (DataGridViewCell Cell in DataGrid_Abon.SelectedCells)
                {
                    if (RowIndex != Cell.RowIndex) { RowIndex = -1; }
                }

                if (RowIndex != -1)
                {
                    FormAbon FormModAbon = new FormAbon(DataGrid_Abon.Rows[RowIndex]);
                    if (FormModAbon.ShowDialog(this) == DialogResult.OK)
                    {
                        System.Media.SystemSounds.Asterisk.Play();

                        Update_DataGrid_Abon();
                    }
                    FormModAbon.Dispose();
                }
                else { MessageBox.Show("Please, select a single record to modify it.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            }
        }

        private void Bn_Abon_Delete_Click(object sender, EventArgs e)
        {
            // Counting rows:
            var RowsToDelete = new List<int>();
            foreach (DataGridViewCell Cell in DataGrid_Abon.SelectedCells)
            {
                int RowId = Cell.RowIndex;
                foreach (int r in RowsToDelete)
                {
                    if (r == RowId) { RowId = -1; }
                }
                if (RowId != -1) { RowsToDelete.Add(RowId); }
            }

            // Asking:
            String NotryText = "Are you sure you want to delete ";
            if (RowsToDelete.Count == 1)
            {
                NotryText += "this record?\nName: " + DataGrid_Abon.Rows[RowsToDelete[0]].Cells["Surname"].Value + ' ' + DataGrid_Abon.Rows[RowsToDelete[0]].Cells["FirstName"].Value + ' ' + DataGrid_Abon.Rows[RowsToDelete[0]].Cells["SecName"].Value;
            }
            else if (RowsToDelete.Count > 1)
            {
                NotryText += RowsToDelete.Count + " selected records?";
            }

            DialogResult Notry = MessageBox.Show(NotryText, "Delete abonent", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (Notry == DialogResult.Yes)
            {
                // Deleting:
                String Request = "DELETE FROM Nikitin_AbonentTable WHERE (";
                for (int i = 0; i < RowsToDelete.Count; i++)
                {
                    if (i > 0) { Request += " OR "; }
                    Request += "(Nikitin_AbonentTable.Id = " + DataGrid_Abon.Rows[RowsToDelete[i]].Cells["Id"].Value + ")";
                }
                Request += ")";

                using (SqlConnection Conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand Doit = new SqlCommand(Request, Conn);
                    Doit.Connection.Open();
                    Doit.ExecuteNonQuery();
                }

                Update_DataGrid_Abon();
                System.Media.SystemSounds.Asterisk.Play();
            }
        }

        private void Bn_Cont_Update_Click(object sender, EventArgs e)
        {
            Update_DataGrid_Cont();
        }

        private void Bn_Cont_New_Click(object sender, EventArgs e)
        {
            FormCont FormNewCont = new FormCont(null);
            if (FormNewCont.ShowDialog(this) == DialogResult.OK)
            {
                System.Media.SystemSounds.Asterisk.Play();

                Update_DataGrid_Cont();
            }
            FormNewCont.Dispose();
        }

        private void Bn_Cont_Modify_Click(object sender, EventArgs e)
        {
            if (DataGrid_Cont.SelectedCells.Count > 0)
            {
                int RowIndex = DataGrid_Cont.SelectedCells[0].RowIndex;
                foreach (DataGridViewCell Cell in DataGrid_Cont.SelectedCells)
                {
                    if (RowIndex != Cell.RowIndex) { RowIndex = -1; }
                }

                if (RowIndex != -1)
                {
                    FormCont FormModCont = new FormCont(DataGrid_Cont.Rows[RowIndex]);
                    if (FormModCont.ShowDialog(this) == DialogResult.OK)
                    {
                        System.Media.SystemSounds.Asterisk.Play();

                        Update_DataGrid_Cont();
                    }
                    FormModCont.Dispose();
                }
                else { MessageBox.Show("Please, select a single record to modify it.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            }
        }

        private void Bn_Cont_Delete_Click(object sender, EventArgs e)
        {
            // Counting rows:
            var RowsToDelete = new List<int>();
            foreach (DataGridViewCell Cell in DataGrid_Cont.SelectedCells)
            {
                int RowId = Cell.RowIndex;
                foreach (int r in RowsToDelete)
                {
                    if (r == RowId) { RowId = -1; }
                }
                if (RowId != -1) { RowsToDelete.Add(RowId); }
            }

            // Asking:
            String NotryText = "Are you sure you want to delete ";
            if (RowsToDelete.Count == 1)
            {
                NotryText += "this record?\nNumber: " + DataGrid_Cont.Rows[RowsToDelete[0]].Cells["Number"].Value;
            }
            else if (RowsToDelete.Count > 1)
            {
                NotryText += RowsToDelete.Count + " selected records?";
            }

            DialogResult Notry = MessageBox.Show(NotryText, "Delete contact", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (Notry == DialogResult.Yes)
            {
                // Deleting:
                String Request = "DELETE FROM Nikitin_ContactTable WHERE (";
                for (int i = 0; i < RowsToDelete.Count; i++)
                {
                    if (i > 0) { Request += " OR "; }
                    Request += "(Nikitin_ContactTable.Id = " + DataGrid_Cont.Rows[RowsToDelete[i]].Cells["Id"].Value + ")";
                }
                Request += ")";

                using (SqlConnection Conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand Doit = new SqlCommand(Request, Conn);
                    Doit.Connection.Open();
                    Doit.ExecuteNonQuery();
                }

                Update_DataGrid_Cont();
                System.Media.SystemSounds.Asterisk.Play();
            }
        }

        private void Bn_Prov_Update_Click(object sender, EventArgs e)
        {
            Update_DataGrid_Prov();
        }

        private void Bn_Prov_New_Click(object sender, EventArgs e)
        {
            FormProv FormNewProv = new FormProv(null);
            if (FormNewProv.ShowDialog(this) == DialogResult.OK)
            {
                System.Media.SystemSounds.Asterisk.Play();

                Update_DataGrid_Prov();
            }
            FormNewProv.Dispose();
        }

        private void Bn_Prov_Modify_Click(object sender, EventArgs e)
        {
            if (DataGrid_Prov.SelectedCells.Count > 0)
            {
                int RowIndex = DataGrid_Prov.SelectedCells[0].RowIndex;
                foreach (DataGridViewCell Cell in DataGrid_Prov.SelectedCells)
                {
                    if (RowIndex != Cell.RowIndex) { RowIndex = -1; }
                }

                if (RowIndex != -1)
                {
                    FormProv FormModProv = new FormProv(DataGrid_Prov.Rows[RowIndex]);
                    if (FormModProv.ShowDialog(this) == DialogResult.OK)
                    {
                        System.Media.SystemSounds.Asterisk.Play();

                        Update_DataGrid_Prov();
                    }
                    FormModProv.Dispose();
                }
                else { MessageBox.Show("Please, select a single record to modify it.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            }
        }

        private void Bn_Prov_Delete_Click(object sender, EventArgs e)
        {
            // Counting rows:
            var RowsToDelete = new List<int>();
            foreach (DataGridViewCell Cell in DataGrid_Prov.SelectedCells)
            {
                int RowId = Cell.RowIndex;
                foreach (int r in RowsToDelete)
                {
                    if (r == RowId) { RowId = -1; }
                }
                if (RowId != -1) { RowsToDelete.Add(RowId); }
            }

            // Asking:
            String NotryText = "Are you sure you want to delete ";
            if (RowsToDelete.Count == 1)
            {
                NotryText += "this record?\nName: " + DataGrid_Prov.Rows[RowsToDelete[0]].Cells["Name"].Value;
            }
            else if (RowsToDelete.Count > 1)
            {
                NotryText += RowsToDelete.Count + " selected records?";
            }

            DialogResult Notry = MessageBox.Show(NotryText, "Delete provider", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (Notry == DialogResult.Yes)
            {
                // Deleting:
                String Request = "DELETE FROM Nikitin_ProviderTable WHERE (";
                for (int i = 0; i < RowsToDelete.Count; i++)
                {
                    if (i > 0) { Request += " OR "; }
                    Request += "(Nikitin_ProviderTable.Id = " + DataGrid_Prov.Rows[RowsToDelete[i]].Cells["Id"].Value + ")";
                }
                Request += ")";

                using (SqlConnection Conn = new SqlConnection(ConnectionString))
                {
                    SqlCommand Doit = new SqlCommand(Request, Conn);
                    Doit.Connection.Open();
                    Doit.ExecuteNonQuery();
                }

                Update_DataGrid_Prov();
                System.Media.SystemSounds.Asterisk.Play();
            }
        }
    }
}
