﻿namespace Data_Phony
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TabCtrl_Tables = new System.Windows.Forms.TabControl();
            this.Tab_Book = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Check_Search_Birthdate = new System.Windows.Forms.CheckBox();
            this.Check_FindAbon = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TB_Number = new System.Windows.Forms.TextBox();
            this.TB_Name = new System.Windows.Forms.TextBox();
            this.TB_Birthdate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DataGrid_FindCont = new System.Windows.Forms.DataGridView();
            this.Tab_Abon = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Bn_Abon_Update = new System.Windows.Forms.Button();
            this.Bn_Abon_Delete = new System.Windows.Forms.Button();
            this.Bn_Abon_Modify = new System.Windows.Forms.Button();
            this.Bn_Abon_New = new System.Windows.Forms.Button();
            this.DataGrid_Abon = new System.Windows.Forms.DataGridView();
            this.Tab_Cont = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.Bn_Cont_Update = new System.Windows.Forms.Button();
            this.Bn_Cont_Delete = new System.Windows.Forms.Button();
            this.Bn_Cont_Modify = new System.Windows.Forms.Button();
            this.Bn_Cont_New = new System.Windows.Forms.Button();
            this.DataGrid_Cont = new System.Windows.Forms.DataGridView();
            this.Tab_Prov = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.Bn_Prov_Update = new System.Windows.Forms.Button();
            this.Bn_Prov_Delete = new System.Windows.Forms.Button();
            this.Bn_Prov_Modify = new System.Windows.Forms.Button();
            this.Bn_Prov_New = new System.Windows.Forms.Button();
            this.DataGrid_Prov = new System.Windows.Forms.DataGridView();
            this.Check_Search_Commentary = new System.Windows.Forms.CheckBox();
            this.TB_Commentary = new System.Windows.Forms.TextBox();
            this.TB_Address = new System.Windows.Forms.TextBox();
            this.Check_Search_Address = new System.Windows.Forms.CheckBox();
            this.TabCtrl_Tables.SuspendLayout();
            this.Tab_Book.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_FindCont)).BeginInit();
            this.Tab_Abon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Abon)).BeginInit();
            this.Tab_Cont.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Cont)).BeginInit();
            this.Tab_Prov.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Prov)).BeginInit();
            this.SuspendLayout();
            // 
            // TabCtrl_Tables
            // 
            this.TabCtrl_Tables.Controls.Add(this.Tab_Book);
            this.TabCtrl_Tables.Controls.Add(this.Tab_Abon);
            this.TabCtrl_Tables.Controls.Add(this.Tab_Cont);
            this.TabCtrl_Tables.Controls.Add(this.Tab_Prov);
            this.TabCtrl_Tables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabCtrl_Tables.ItemSize = new System.Drawing.Size(155, 18);
            this.TabCtrl_Tables.Location = new System.Drawing.Point(0, 0);
            this.TabCtrl_Tables.Margin = new System.Windows.Forms.Padding(0);
            this.TabCtrl_Tables.Name = "TabCtrl_Tables";
            this.TabCtrl_Tables.SelectedIndex = 0;
            this.TabCtrl_Tables.Size = new System.Drawing.Size(712, 442);
            this.TabCtrl_Tables.TabIndex = 1;
            // 
            // Tab_Book
            // 
            this.Tab_Book.Controls.Add(this.splitContainer1);
            this.Tab_Book.Location = new System.Drawing.Point(4, 22);
            this.Tab_Book.Margin = new System.Windows.Forms.Padding(0);
            this.Tab_Book.Name = "Tab_Book";
            this.Tab_Book.Size = new System.Drawing.Size(704, 416);
            this.Tab_Book.TabIndex = 0;
            this.Tab_Book.Text = "Search";
            this.Tab_Book.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Check_Search_Address);
            this.splitContainer1.Panel1.Controls.Add(this.Check_Search_Commentary);
            this.splitContainer1.Panel1.Controls.Add(this.Check_Search_Birthdate);
            this.splitContainer1.Panel1.Controls.Add(this.Check_FindAbon);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.TB_Number);
            this.splitContainer1.Panel1.Controls.Add(this.TB_Name);
            this.splitContainer1.Panel1.Controls.Add(this.TB_Address);
            this.splitContainer1.Panel1.Controls.Add(this.TB_Commentary);
            this.splitContainer1.Panel1.Controls.Add(this.TB_Birthdate);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.DataGrid_FindCont);
            this.splitContainer1.Size = new System.Drawing.Size(704, 416);
            this.splitContainer1.SplitterDistance = 76;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 1;
            // 
            // Check_Search_Birthdate
            // 
            this.Check_Search_Birthdate.Location = new System.Drawing.Point(257, 4);
            this.Check_Search_Birthdate.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Check_Search_Birthdate.Name = "Check_Search_Birthdate";
            this.Check_Search_Birthdate.Size = new System.Drawing.Size(68, 20);
            this.Check_Search_Birthdate.TabIndex = 41;
            this.Check_Search_Birthdate.Text = "Birthdate";
            this.Check_Search_Birthdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Check_Search_Birthdate.UseVisualStyleBackColor = true;
            this.Check_Search_Birthdate.CheckedChanged += new System.EventHandler(this.Check_Search_Birthdate_CheckedChanged);
            // 
            // Check_FindAbon
            // 
            this.Check_FindAbon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Check_FindAbon.Appearance = System.Windows.Forms.Appearance.Button;
            this.Check_FindAbon.Location = new System.Drawing.Point(640, 4);
            this.Check_FindAbon.Margin = new System.Windows.Forms.Padding(4);
            this.Check_FindAbon.Name = "Check_FindAbon";
            this.Check_FindAbon.Size = new System.Drawing.Size(60, 68);
            this.Check_FindAbon.TabIndex = 38;
            this.Check_FindAbon.Text = "Search";
            this.Check_FindAbon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Check_FindAbon.UseVisualStyleBackColor = true;
            this.Check_FindAbon.CheckedChanged += new System.EventHandler(this.Check_FindAbon_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 37;
            this.label1.Text = "Name:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_Number
            // 
            this.TB_Number.Location = new System.Drawing.Point(56, 28);
            this.TB_Number.Margin = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.TB_Number.Name = "TB_Number";
            this.TB_Number.Size = new System.Drawing.Size(80, 20);
            this.TB_Number.TabIndex = 40;
            this.TB_Number.TextChanged += new System.EventHandler(this.TB_Number_TextChanged);
            // 
            // TB_Name
            // 
            this.TB_Name.Location = new System.Drawing.Point(56, 4);
            this.TB_Name.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Name.Name = "TB_Name";
            this.TB_Name.Size = new System.Drawing.Size(193, 20);
            this.TB_Name.TabIndex = 35;
            this.TB_Name.TextChanged += new System.EventHandler(this.TB_Name_TextChanged);
            // 
            // TB_Birthdate
            // 
            this.TB_Birthdate.Location = new System.Drawing.Point(329, 4);
            this.TB_Birthdate.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Birthdate.Name = "TB_Birthdate";
            this.TB_Birthdate.Size = new System.Drawing.Size(64, 20);
            this.TB_Birthdate.TabIndex = 39;
            this.TB_Birthdate.Visible = false;
            this.TB_Birthdate.TextChanged += new System.EventHandler(this.TB_Birthdate_TextChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 28);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 0, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 20);
            this.label4.TabIndex = 36;
            this.label4.Text = "Number:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DataGrid_FindCont
            // 
            this.DataGrid_FindCont.AllowUserToAddRows = false;
            this.DataGrid_FindCont.AllowUserToDeleteRows = false;
            this.DataGrid_FindCont.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_FindCont.DefaultCellStyle = dataGridViewCellStyle21;
            this.DataGrid_FindCont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_FindCont.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_FindCont.Name = "DataGrid_FindCont";
            this.DataGrid_FindCont.ReadOnly = true;
            this.DataGrid_FindCont.Size = new System.Drawing.Size(704, 338);
            this.DataGrid_FindCont.TabIndex = 34;
            // 
            // Tab_Abon
            // 
            this.Tab_Abon.Controls.Add(this.splitContainer2);
            this.Tab_Abon.Location = new System.Drawing.Point(4, 22);
            this.Tab_Abon.Margin = new System.Windows.Forms.Padding(0);
            this.Tab_Abon.Name = "Tab_Abon";
            this.Tab_Abon.Size = new System.Drawing.Size(704, 416);
            this.Tab_Abon.TabIndex = 1;
            this.Tab_Abon.Text = "Abonents";
            this.Tab_Abon.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Bn_Abon_Update);
            this.splitContainer2.Panel1.Controls.Add(this.Bn_Abon_Delete);
            this.splitContainer2.Panel1.Controls.Add(this.Bn_Abon_Modify);
            this.splitContainer2.Panel1.Controls.Add(this.Bn_Abon_New);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.DataGrid_Abon);
            this.splitContainer2.Size = new System.Drawing.Size(704, 416);
            this.splitContainer2.SplitterDistance = 32;
            this.splitContainer2.TabIndex = 0;
            // 
            // Bn_Abon_Update
            // 
            this.Bn_Abon_Update.Location = new System.Drawing.Point(4, 4);
            this.Bn_Abon_Update.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Abon_Update.Name = "Bn_Abon_Update";
            this.Bn_Abon_Update.Size = new System.Drawing.Size(64, 24);
            this.Bn_Abon_Update.TabIndex = 0;
            this.Bn_Abon_Update.Text = "Update";
            this.Bn_Abon_Update.UseVisualStyleBackColor = true;
            this.Bn_Abon_Update.Click += new System.EventHandler(this.Bn_Abon_Update_Click);
            // 
            // Bn_Abon_Delete
            // 
            this.Bn_Abon_Delete.Location = new System.Drawing.Point(220, 4);
            this.Bn_Abon_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Abon_Delete.Name = "Bn_Abon_Delete";
            this.Bn_Abon_Delete.Size = new System.Drawing.Size(64, 24);
            this.Bn_Abon_Delete.TabIndex = 0;
            this.Bn_Abon_Delete.Text = "Delete";
            this.Bn_Abon_Delete.UseVisualStyleBackColor = true;
            this.Bn_Abon_Delete.Click += new System.EventHandler(this.Bn_Abon_Delete_Click);
            // 
            // Bn_Abon_Modify
            // 
            this.Bn_Abon_Modify.Location = new System.Drawing.Point(148, 4);
            this.Bn_Abon_Modify.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Abon_Modify.Name = "Bn_Abon_Modify";
            this.Bn_Abon_Modify.Size = new System.Drawing.Size(64, 24);
            this.Bn_Abon_Modify.TabIndex = 0;
            this.Bn_Abon_Modify.Text = "Modify";
            this.Bn_Abon_Modify.UseVisualStyleBackColor = true;
            this.Bn_Abon_Modify.Click += new System.EventHandler(this.Bn_Abon_Modify_Click);
            // 
            // Bn_Abon_New
            // 
            this.Bn_Abon_New.Location = new System.Drawing.Point(76, 4);
            this.Bn_Abon_New.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Abon_New.Name = "Bn_Abon_New";
            this.Bn_Abon_New.Size = new System.Drawing.Size(64, 24);
            this.Bn_Abon_New.TabIndex = 0;
            this.Bn_Abon_New.Text = "New";
            this.Bn_Abon_New.UseVisualStyleBackColor = true;
            this.Bn_Abon_New.Click += new System.EventHandler(this.Bn_Abon_New_Click);
            // 
            // DataGrid_Abon
            // 
            this.DataGrid_Abon.AllowUserToAddRows = false;
            this.DataGrid_Abon.AllowUserToDeleteRows = false;
            this.DataGrid_Abon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_Abon.DefaultCellStyle = dataGridViewCellStyle22;
            this.DataGrid_Abon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_Abon.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_Abon.Name = "DataGrid_Abon";
            this.DataGrid_Abon.ReadOnly = true;
            this.DataGrid_Abon.Size = new System.Drawing.Size(704, 380);
            this.DataGrid_Abon.TabIndex = 35;
            // 
            // Tab_Cont
            // 
            this.Tab_Cont.Controls.Add(this.splitContainer3);
            this.Tab_Cont.Location = new System.Drawing.Point(4, 22);
            this.Tab_Cont.Margin = new System.Windows.Forms.Padding(0);
            this.Tab_Cont.Name = "Tab_Cont";
            this.Tab_Cont.Size = new System.Drawing.Size(704, 416);
            this.Tab_Cont.TabIndex = 2;
            this.Tab_Cont.Text = "Contacts";
            this.Tab_Cont.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.Bn_Cont_Update);
            this.splitContainer3.Panel1.Controls.Add(this.Bn_Cont_Delete);
            this.splitContainer3.Panel1.Controls.Add(this.Bn_Cont_Modify);
            this.splitContainer3.Panel1.Controls.Add(this.Bn_Cont_New);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.DataGrid_Cont);
            this.splitContainer3.Size = new System.Drawing.Size(704, 416);
            this.splitContainer3.SplitterDistance = 32;
            this.splitContainer3.TabIndex = 1;
            // 
            // Bn_Cont_Update
            // 
            this.Bn_Cont_Update.Location = new System.Drawing.Point(4, 4);
            this.Bn_Cont_Update.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cont_Update.Name = "Bn_Cont_Update";
            this.Bn_Cont_Update.Size = new System.Drawing.Size(64, 24);
            this.Bn_Cont_Update.TabIndex = 0;
            this.Bn_Cont_Update.Text = "Update";
            this.Bn_Cont_Update.UseVisualStyleBackColor = true;
            this.Bn_Cont_Update.Click += new System.EventHandler(this.Bn_Cont_Update_Click);
            // 
            // Bn_Cont_Delete
            // 
            this.Bn_Cont_Delete.Location = new System.Drawing.Point(220, 4);
            this.Bn_Cont_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cont_Delete.Name = "Bn_Cont_Delete";
            this.Bn_Cont_Delete.Size = new System.Drawing.Size(64, 24);
            this.Bn_Cont_Delete.TabIndex = 0;
            this.Bn_Cont_Delete.Text = "Delete";
            this.Bn_Cont_Delete.UseVisualStyleBackColor = true;
            this.Bn_Cont_Delete.Click += new System.EventHandler(this.Bn_Cont_Delete_Click);
            // 
            // Bn_Cont_Modify
            // 
            this.Bn_Cont_Modify.Location = new System.Drawing.Point(148, 4);
            this.Bn_Cont_Modify.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cont_Modify.Name = "Bn_Cont_Modify";
            this.Bn_Cont_Modify.Size = new System.Drawing.Size(64, 24);
            this.Bn_Cont_Modify.TabIndex = 0;
            this.Bn_Cont_Modify.Text = "Modify";
            this.Bn_Cont_Modify.UseVisualStyleBackColor = true;
            this.Bn_Cont_Modify.Click += new System.EventHandler(this.Bn_Cont_Modify_Click);
            // 
            // Bn_Cont_New
            // 
            this.Bn_Cont_New.Location = new System.Drawing.Point(76, 4);
            this.Bn_Cont_New.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cont_New.Name = "Bn_Cont_New";
            this.Bn_Cont_New.Size = new System.Drawing.Size(64, 24);
            this.Bn_Cont_New.TabIndex = 0;
            this.Bn_Cont_New.Text = "New";
            this.Bn_Cont_New.UseVisualStyleBackColor = true;
            this.Bn_Cont_New.Click += new System.EventHandler(this.Bn_Cont_New_Click);
            // 
            // DataGrid_Cont
            // 
            this.DataGrid_Cont.AllowUserToAddRows = false;
            this.DataGrid_Cont.AllowUserToDeleteRows = false;
            this.DataGrid_Cont.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_Cont.DefaultCellStyle = dataGridViewCellStyle23;
            this.DataGrid_Cont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_Cont.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_Cont.Name = "DataGrid_Cont";
            this.DataGrid_Cont.ReadOnly = true;
            this.DataGrid_Cont.Size = new System.Drawing.Size(704, 380);
            this.DataGrid_Cont.TabIndex = 35;
            // 
            // Tab_Prov
            // 
            this.Tab_Prov.Controls.Add(this.splitContainer4);
            this.Tab_Prov.Location = new System.Drawing.Point(4, 22);
            this.Tab_Prov.Margin = new System.Windows.Forms.Padding(0);
            this.Tab_Prov.Name = "Tab_Prov";
            this.Tab_Prov.Size = new System.Drawing.Size(704, 416);
            this.Tab_Prov.TabIndex = 3;
            this.Tab_Prov.Text = "Providers";
            this.Tab_Prov.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.Bn_Prov_Update);
            this.splitContainer4.Panel1.Controls.Add(this.Bn_Prov_Delete);
            this.splitContainer4.Panel1.Controls.Add(this.Bn_Prov_Modify);
            this.splitContainer4.Panel1.Controls.Add(this.Bn_Prov_New);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.DataGrid_Prov);
            this.splitContainer4.Size = new System.Drawing.Size(704, 416);
            this.splitContainer4.SplitterDistance = 32;
            this.splitContainer4.TabIndex = 1;
            // 
            // Bn_Prov_Update
            // 
            this.Bn_Prov_Update.Location = new System.Drawing.Point(4, 4);
            this.Bn_Prov_Update.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Prov_Update.Name = "Bn_Prov_Update";
            this.Bn_Prov_Update.Size = new System.Drawing.Size(64, 24);
            this.Bn_Prov_Update.TabIndex = 0;
            this.Bn_Prov_Update.Text = "Update";
            this.Bn_Prov_Update.UseVisualStyleBackColor = true;
            this.Bn_Prov_Update.Click += new System.EventHandler(this.Bn_Prov_Update_Click);
            // 
            // Bn_Prov_Delete
            // 
            this.Bn_Prov_Delete.Location = new System.Drawing.Point(220, 4);
            this.Bn_Prov_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Prov_Delete.Name = "Bn_Prov_Delete";
            this.Bn_Prov_Delete.Size = new System.Drawing.Size(64, 24);
            this.Bn_Prov_Delete.TabIndex = 0;
            this.Bn_Prov_Delete.Text = "Delete";
            this.Bn_Prov_Delete.UseVisualStyleBackColor = true;
            this.Bn_Prov_Delete.Click += new System.EventHandler(this.Bn_Prov_Delete_Click);
            // 
            // Bn_Prov_Modify
            // 
            this.Bn_Prov_Modify.Location = new System.Drawing.Point(148, 4);
            this.Bn_Prov_Modify.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Prov_Modify.Name = "Bn_Prov_Modify";
            this.Bn_Prov_Modify.Size = new System.Drawing.Size(64, 24);
            this.Bn_Prov_Modify.TabIndex = 0;
            this.Bn_Prov_Modify.Text = "Modify";
            this.Bn_Prov_Modify.UseVisualStyleBackColor = true;
            this.Bn_Prov_Modify.Click += new System.EventHandler(this.Bn_Prov_Modify_Click);
            // 
            // Bn_Prov_New
            // 
            this.Bn_Prov_New.Location = new System.Drawing.Point(76, 4);
            this.Bn_Prov_New.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Prov_New.Name = "Bn_Prov_New";
            this.Bn_Prov_New.Size = new System.Drawing.Size(64, 24);
            this.Bn_Prov_New.TabIndex = 0;
            this.Bn_Prov_New.Text = "New";
            this.Bn_Prov_New.UseVisualStyleBackColor = true;
            this.Bn_Prov_New.Click += new System.EventHandler(this.Bn_Prov_New_Click);
            // 
            // DataGrid_Prov
            // 
            this.DataGrid_Prov.AllowUserToAddRows = false;
            this.DataGrid_Prov.AllowUserToDeleteRows = false;
            this.DataGrid_Prov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_Prov.DefaultCellStyle = dataGridViewCellStyle24;
            this.DataGrid_Prov.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_Prov.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_Prov.Name = "DataGrid_Prov";
            this.DataGrid_Prov.ReadOnly = true;
            this.DataGrid_Prov.Size = new System.Drawing.Size(704, 380);
            this.DataGrid_Prov.TabIndex = 35;
            // 
            // Check_Search_Commentary
            // 
            this.Check_Search_Commentary.Location = new System.Drawing.Point(9, 51);
            this.Check_Search_Commentary.Margin = new System.Windows.Forms.Padding(4, 0, 0, 4);
            this.Check_Search_Commentary.Name = "Check_Search_Commentary";
            this.Check_Search_Commentary.Size = new System.Drawing.Size(84, 20);
            this.Check_Search_Commentary.TabIndex = 41;
            this.Check_Search_Commentary.Text = "Commentary";
            this.Check_Search_Commentary.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Check_Search_Commentary.UseVisualStyleBackColor = true;
            this.Check_Search_Commentary.CheckedChanged += new System.EventHandler(this.Check_Search_Commentary_CheckedChanged);
            // 
            // TB_Commentary
            // 
            this.TB_Commentary.Location = new System.Drawing.Point(97, 51);
            this.TB_Commentary.Margin = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.TB_Commentary.Multiline = true;
            this.TB_Commentary.Name = "TB_Commentary";
            this.TB_Commentary.Size = new System.Drawing.Size(296, 20);
            this.TB_Commentary.TabIndex = 39;
            this.TB_Commentary.Visible = false;
            this.TB_Commentary.TextChanged += new System.EventHandler(this.TB_Commentary_TextChanged);
            // 
            // TB_Address
            // 
            this.TB_Address.Location = new System.Drawing.Point(212, 28);
            this.TB_Address.Margin = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.TB_Address.Multiline = true;
            this.TB_Address.Name = "TB_Address";
            this.TB_Address.Size = new System.Drawing.Size(181, 20);
            this.TB_Address.TabIndex = 39;
            this.TB_Address.Visible = false;
            this.TB_Address.TextChanged += new System.EventHandler(this.TB_Address_TextChanged);
            // 
            // Check_Search_Address
            // 
            this.Check_Search_Address.Location = new System.Drawing.Point(144, 29);
            this.Check_Search_Address.Margin = new System.Windows.Forms.Padding(4, 0, 0, 4);
            this.Check_Search_Address.Name = "Check_Search_Address";
            this.Check_Search_Address.Size = new System.Drawing.Size(64, 20);
            this.Check_Search_Address.TabIndex = 41;
            this.Check_Search_Address.Text = "Address";
            this.Check_Search_Address.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Check_Search_Address.UseVisualStyleBackColor = true;
            this.Check_Search_Address.CheckedChanged += new System.EventHandler(this.Check_Search_Address_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 442);
            this.Controls.Add(this.TabCtrl_Tables);
            this.MinimumSize = new System.Drawing.Size(480, 240);
            this.Name = "Form1";
            this.Text = "Phone book";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TabCtrl_Tables.ResumeLayout(false);
            this.Tab_Book.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_FindCont)).EndInit();
            this.Tab_Abon.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Abon)).EndInit();
            this.Tab_Cont.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Cont)).EndInit();
            this.Tab_Prov.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Prov)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabCtrl_Tables;
        private System.Windows.Forms.TabPage Tab_Book;
        private System.Windows.Forms.TabPage Tab_Abon;
        private System.Windows.Forms.TabPage Tab_Cont;
        private System.Windows.Forms.TabPage Tab_Prov;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox Check_Search_Birthdate;
        private System.Windows.Forms.CheckBox Check_FindAbon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB_Number;
        private System.Windows.Forms.TextBox TB_Name;
        private System.Windows.Forms.TextBox TB_Birthdate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView DataGrid_FindCont;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button Bn_Abon_Update;
        private System.Windows.Forms.Button Bn_Abon_Delete;
        private System.Windows.Forms.Button Bn_Abon_Modify;
        private System.Windows.Forms.Button Bn_Abon_New;
        private System.Windows.Forms.DataGridView DataGrid_Abon;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button Bn_Cont_Update;
        private System.Windows.Forms.Button Bn_Cont_Delete;
        private System.Windows.Forms.Button Bn_Cont_Modify;
        private System.Windows.Forms.Button Bn_Cont_New;
        private System.Windows.Forms.DataGridView DataGrid_Cont;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Button Bn_Prov_Update;
        private System.Windows.Forms.Button Bn_Prov_Delete;
        private System.Windows.Forms.Button Bn_Prov_Modify;
        private System.Windows.Forms.Button Bn_Prov_New;
        private System.Windows.Forms.DataGridView DataGrid_Prov;
        private System.Windows.Forms.CheckBox Check_Search_Commentary;
        private System.Windows.Forms.TextBox TB_Commentary;
        private System.Windows.Forms.CheckBox Check_Search_Address;
        private System.Windows.Forms.TextBox TB_Address;
    }
}

