﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data_Phony
{
    public partial class FormAbon : Form
    {
        private int RowId;
        private String Request;

        public FormAbon(DataGridViewRow Row)
        {
            InitializeComponent();
            this.Hide();

            if (Row == null)
            {
                this.Text = "Create new abonent record";
                Bn_LinkView.Enabled = false;
                RowId = 0;
            }
            else
            {
                this.Text = "Modify abonent record";
                Bn_LinkView.Enabled = true;
                RowId = (int)(Row.Cells["Id"].Value);

                TB_AE_Surname.Text = Row.Cells["SurName"].Value.ToString();
                TB_AE_Firstname.Text = Row.Cells["FirstName"].Value.ToString();
                TB_AE_Secondname.Text = Row.Cells["SecName"].Value.ToString();
                TB_AE_Birthdate.Text = Row.Cells["BirthDate"].Value.ToString();
                TB_AE_Address.Text = Row.Cells["Address"].Value.ToString();
                TB_AE_Commentary.Text = Row.Cells["Commentary"].Value.ToString();
            }
        }

        private void Bn_LinkView_Click(object sender, EventArgs e)
        {
            // View all contacts related to this abonent:
            string Linkage = @"SELECT Nikitin_CARelationTable.ContactId, Nikitin_ContactTable.Number 
                FROM (Nikitin_CARelationTable RIGHT JOIN Nikitin_ContactTable ON (Nikitin_CARelationTable.ContactId = Nikitin_ContactTable.Id)) 
                WHERE (Nikitin_CARelationTable.AbonentId = " + RowId + ")";
            var Adapter = new SqlDataAdapter(Linkage, Form1.ConnectionString);
            var LinkTable = new DataTable();
            Adapter.Fill(LinkTable);

            DataGrid_AbonLink.Columns.Add("ContactId", "ContactId");
            DataGrid_AbonLink.Columns.Add("Number", "Number");
            foreach (DataRow Row in LinkTable.Rows)
            {
                DataGrid_AbonLink.Rows.Add(Row.ItemArray);
            }
            DataGrid_AbonLink.Columns["ContactId"].Visible = false;
        }

        private void Bn_LinkNew_Click(object sender, EventArgs e)
        {
            // Create new contact record:
            FormCont FormNewCont = new FormCont(null);
            if (FormNewCont.ShowDialog(this) == DialogResult.OK)
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
            FormNewCont.Dispose();
        }

        private void Bn_LinkExist_Click(object sender, EventArgs e)
        {
            // Link existing contact to this abonent:
            string Linkage = @"SELECT Nikitin_ContactTable.Id AS ContactId, Nikitin_ContactTable.Number FROM Nikitin_ContactTable
                WHERE (Nikitin_ContactTable.Number LIKE '" + TB_LinkNumber.Text + "')";
            var Adapter = new SqlDataAdapter(Linkage, Form1.ConnectionString);
            var LinkTable = new DataTable();
            Adapter.Fill(LinkTable);
            if (DataGrid_AbonLink.ColumnCount == 0)
            {
                DataGrid_AbonLink.Columns.Add("ContactId", "ContactId");
                DataGrid_AbonLink.Columns.Add("Number", "Number");
            }
            if (LinkTable.Rows.Count > 0)
            {
                DataGrid_AbonLink.Rows.Add(LinkTable.Rows[0].ItemArray);
            }
            DataGrid_AbonLink.Columns["ContactId"].Visible = false;
        }

        private void Bn_LinkDelete_Click(object sender, EventArgs e)
        {
            // Unlink selected abonent from this contact:
            DataGrid_AbonLink.Rows.RemoveAt(DataGrid_AbonLink.SelectedCells[0].RowIndex);
        }

        private void Bn_OK_Click(object sender, EventArgs e)
        {
            if (RowId == 0)
            {
                // Create new abonent:
                Request = @"INSERT INTO Nikitin_AbonentTable (SurName, FirstName, SecName, BirthDate, Address, Commentary)
                    VALUES ('" + TB_AE_Surname.Text + "', '" + TB_AE_Firstname.Text + "', '" + TB_AE_Secondname.Text + @"'
                    , CAST('" + TB_AE_Birthdate.Text + "' AS datetime), '" + TB_AE_Address.Text + "', '" + TB_AE_Commentary.Text + "')";
            }
            else
            {
                // Modify abonent:
                Request = @"UPDATE Nikitin_AbonentTable 
                    SET SurName = '" + TB_AE_Surname.Text + "', FirstName = '" + TB_AE_Firstname.Text + "', SecName = '" + TB_AE_Secondname.Text + @"', 
                    BirthDate = CONVERT(datetime, '" + TB_AE_Birthdate.Text + "', 104), Address = '" + TB_AE_Address.Text + "', Commentary = '" + TB_AE_Commentary.Text + @"'
                    WHERE (Id = " + RowId + ")";
            }

            using (SqlConnection Conn = new SqlConnection(Form1.ConnectionString))
            {
                SqlCommand Doit = new SqlCommand(Request, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
            }

            Request = @"SELECT Nikitin_AbonentTable.Id FROM Nikitin_AbonentTable
                WHERE (Nikitin_AbonentTable.FirstName LIKE '" + TB_AE_Firstname.Text + "')";
            var Adapter = new SqlDataAdapter(Request, Form1.ConnectionString);
            var Table = new DataTable();
            Adapter.Fill(Table);
            RowId = (int)(Table.Rows[0].ItemArray[0]);

            // Update relation table:
            if (DataGrid_AbonLink.Rows.Count > 0)
            {
                Request += @"
                DELETE FROM Nikitin_CARelationTable WHERE (Nikitin_CARelationTable.AbonentId = " + RowId + @")
                INSERT INTO Nikitin_CARelationTable (AbonentId, ContactId) VALUES";
                for (int i = 0; i < DataGrid_AbonLink.Rows.Count; i++)
                {
                    if (i > 0) { Request += ","; }
                    Request += " (" + RowId + ", " + DataGrid_AbonLink.Rows[i].Cells["ContactId"].Value + ")";
                }
            }

            using (SqlConnection Conn = new SqlConnection(Form1.ConnectionString))
            {
                SqlCommand Doit = new SqlCommand(Request, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
            }

            this.Close();
        }

        private void Bn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
