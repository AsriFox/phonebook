﻿namespace Data_Phony
{
    partial class FormCont
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.LB_CE_Type = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Bn_CE_NewProv = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_CE_Number = new System.Windows.Forms.TextBox();
            this.LB_CE_Provider = new System.Windows.Forms.ComboBox();
            this.DataGrid_Prov_Invisible = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.DataGrid_ContLink = new System.Windows.Forms.DataGridView();
            this.TB_LinkName = new System.Windows.Forms.TextBox();
            this.Bn_LinkView = new System.Windows.Forms.Button();
            this.Bn_LinkNew = new System.Windows.Forms.Button();
            this.Bn_LinkDelete = new System.Windows.Forms.Button();
            this.Bn_LinkExist = new System.Windows.Forms.Button();
            this.Bn_Cancel = new System.Windows.Forms.Button();
            this.Bn_OK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Prov_Invisible)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_ContLink)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Bn_Cancel);
            this.splitContainer1.Panel2.Controls.Add(this.Bn_OK);
            this.splitContainer1.Panel2MinSize = 28;
            this.splitContainer1.Size = new System.Drawing.Size(312, 293);
            this.splitContainer1.SplitterDistance = 264;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(312, 264);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(304, 238);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.Controls.Add(this.LB_CE_Type, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Bn_CE_NewProv, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.TB_CE_Number, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.LB_CE_Provider, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.DataGrid_Prov_Invisible, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(304, 238);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // LB_CE_Type
            // 
            this.LB_CE_Type.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LB_CE_Type.FormattingEnabled = true;
            this.LB_CE_Type.Items.AddRange(new object[] {
            "Home",
            "Mobile",
            "Work"});
            this.LB_CE_Type.Location = new System.Drawing.Point(68, 32);
            this.LB_CE_Type.Margin = new System.Windows.Forms.Padding(4);
            this.LB_CE_Type.MaxDropDownItems = 3;
            this.LB_CE_Type.Name = "LB_CE_Type";
            this.LB_CE_Type.Size = new System.Drawing.Size(168, 21);
            this.LB_CE_Type.Sorted = true;
            this.LB_CE_Type.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Provider:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Bn_CE_NewProv
            // 
            this.Bn_CE_NewProv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_CE_NewProv.Location = new System.Drawing.Point(244, 60);
            this.Bn_CE_NewProv.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_CE_NewProv.Name = "Bn_CE_NewProv";
            this.Bn_CE_NewProv.Size = new System.Drawing.Size(56, 20);
            this.Bn_CE_NewProv.TabIndex = 0;
            this.Bn_CE_NewProv.Text = "New";
            this.Bn_CE_NewProv.UseVisualStyleBackColor = true;
            this.Bn_CE_NewProv.Click += new System.EventHandler(this.Bn_CE_NewProv_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Type:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Number:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_CE_Number
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_CE_Number, 2);
            this.TB_CE_Number.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_CE_Number.Location = new System.Drawing.Point(68, 4);
            this.TB_CE_Number.Margin = new System.Windows.Forms.Padding(4);
            this.TB_CE_Number.Name = "TB_CE_Number";
            this.TB_CE_Number.Size = new System.Drawing.Size(232, 20);
            this.TB_CE_Number.TabIndex = 0;
            // 
            // LB_CE_Provider
            // 
            this.LB_CE_Provider.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LB_CE_Provider.FormattingEnabled = true;
            this.LB_CE_Provider.Location = new System.Drawing.Point(68, 60);
            this.LB_CE_Provider.Margin = new System.Windows.Forms.Padding(4);
            this.LB_CE_Provider.Name = "LB_CE_Provider";
            this.LB_CE_Provider.Size = new System.Drawing.Size(168, 21);
            this.LB_CE_Provider.Sorted = true;
            this.LB_CE_Provider.TabIndex = 13;
            // 
            // DataGrid_Prov_Invisible
            // 
            this.DataGrid_Prov_Invisible.AllowUserToAddRows = false;
            this.DataGrid_Prov_Invisible.AllowUserToDeleteRows = false;
            this.DataGrid_Prov_Invisible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid_Prov_Invisible.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_Prov_Invisible.Enabled = false;
            this.DataGrid_Prov_Invisible.Location = new System.Drawing.Point(67, 87);
            this.DataGrid_Prov_Invisible.Name = "DataGrid_Prov_Invisible";
            this.DataGrid_Prov_Invisible.Size = new System.Drawing.Size(170, 148);
            this.DataGrid_Prov_Invisible.TabIndex = 15;
            this.DataGrid_Prov_Invisible.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(304, 238);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Abonents";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.DataGrid_ContLink);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.TB_LinkName);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkView);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkNew);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkDelete);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkExist);
            this.splitContainer2.Size = new System.Drawing.Size(304, 238);
            this.splitContainer2.SplitterDistance = 186;
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 0;
            // 
            // DataGrid_ContLink
            // 
            this.DataGrid_ContLink.AllowUserToAddRows = false;
            this.DataGrid_ContLink.AllowUserToDeleteRows = false;
            this.DataGrid_ContLink.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid_ContLink.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_ContLink.DefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid_ContLink.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_ContLink.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_ContLink.Margin = new System.Windows.Forms.Padding(0);
            this.DataGrid_ContLink.MultiSelect = false;
            this.DataGrid_ContLink.Name = "DataGrid_ContLink";
            this.DataGrid_ContLink.ReadOnly = true;
            this.DataGrid_ContLink.RowHeadersVisible = false;
            this.DataGrid_ContLink.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.DataGrid_ContLink.RowTemplate.Height = 20;
            this.DataGrid_ContLink.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DataGrid_ContLink.Size = new System.Drawing.Size(304, 186);
            this.DataGrid_ContLink.TabIndex = 35;
            // 
            // TB_LinkName
            // 
            this.TB_LinkName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TB_LinkName.Location = new System.Drawing.Point(4, 28);
            this.TB_LinkName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.TB_LinkName.Name = "TB_LinkName";
            this.TB_LinkName.Size = new System.Drawing.Size(296, 20);
            this.TB_LinkName.TabIndex = 1;
            // 
            // Bn_LinkView
            // 
            this.Bn_LinkView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkView.Location = new System.Drawing.Point(4, 4);
            this.Bn_LinkView.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkView.Name = "Bn_LinkView";
            this.Bn_LinkView.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkView.TabIndex = 0;
            this.Bn_LinkView.Text = "View";
            this.Bn_LinkView.UseVisualStyleBackColor = true;
            this.Bn_LinkView.Click += new System.EventHandler(this.Bn_LinkView_Click);
            // 
            // Bn_LinkNew
            // 
            this.Bn_LinkNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkNew.Location = new System.Drawing.Point(48, 4);
            this.Bn_LinkNew.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkNew.Name = "Bn_LinkNew";
            this.Bn_LinkNew.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkNew.TabIndex = 0;
            this.Bn_LinkNew.Text = "New";
            this.Bn_LinkNew.UseVisualStyleBackColor = true;
            this.Bn_LinkNew.Click += new System.EventHandler(this.Bn_LinkNew_Click);
            // 
            // Bn_LinkDelete
            // 
            this.Bn_LinkDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkDelete.Location = new System.Drawing.Point(136, 4);
            this.Bn_LinkDelete.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkDelete.Name = "Bn_LinkDelete";
            this.Bn_LinkDelete.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkDelete.TabIndex = 0;
            this.Bn_LinkDelete.Text = "Del";
            this.Bn_LinkDelete.UseVisualStyleBackColor = true;
            this.Bn_LinkDelete.Click += new System.EventHandler(this.Bn_LinkDelete_Click);
            // 
            // Bn_LinkExist
            // 
            this.Bn_LinkExist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkExist.Location = new System.Drawing.Point(92, 4);
            this.Bn_LinkExist.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkExist.Name = "Bn_LinkExist";
            this.Bn_LinkExist.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkExist.TabIndex = 0;
            this.Bn_LinkExist.Text = "Link";
            this.Bn_LinkExist.UseVisualStyleBackColor = true;
            this.Bn_LinkExist.Click += new System.EventHandler(this.Bn_LinkExist_Click);
            // 
            // Bn_Cancel
            // 
            this.Bn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Bn_Cancel.Location = new System.Drawing.Point(244, 4);
            this.Bn_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cancel.Name = "Bn_Cancel";
            this.Bn_Cancel.Size = new System.Drawing.Size(64, 20);
            this.Bn_Cancel.TabIndex = 0;
            this.Bn_Cancel.Text = "Cancel";
            this.Bn_Cancel.UseVisualStyleBackColor = true;
            this.Bn_Cancel.Click += new System.EventHandler(this.Bn_Cancel_Click);
            // 
            // Bn_OK
            // 
            this.Bn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Bn_OK.Location = new System.Drawing.Point(172, 4);
            this.Bn_OK.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_OK.Name = "Bn_OK";
            this.Bn_OK.Size = new System.Drawing.Size(64, 20);
            this.Bn_OK.TabIndex = 0;
            this.Bn_OK.Text = "Submit";
            this.Bn_OK.UseVisualStyleBackColor = true;
            this.Bn_OK.Click += new System.EventHandler(this.Bn_OK_Click);
            // 
            // FormCont
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 293);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(240, 320);
            this.Name = "FormCont";
            this.Text = "Contact";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Prov_Invisible)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_ContLink)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_CE_Number;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView DataGrid_ContLink;
        private System.Windows.Forms.TextBox TB_LinkName;
        private System.Windows.Forms.Button Bn_LinkView;
        private System.Windows.Forms.Button Bn_LinkNew;
        private System.Windows.Forms.Button Bn_LinkExist;
        private System.Windows.Forms.Button Bn_Cancel;
        private System.Windows.Forms.Button Bn_OK;
        private System.Windows.Forms.ComboBox LB_CE_Type;
        private System.Windows.Forms.Button Bn_CE_NewProv;
        private System.Windows.Forms.ComboBox LB_CE_Provider;
        private System.Windows.Forms.DataGridView DataGrid_Prov_Invisible;
        private System.Windows.Forms.Button Bn_LinkDelete;
    }
}