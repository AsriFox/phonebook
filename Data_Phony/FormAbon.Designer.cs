﻿namespace Data_Phony
{
    partial class FormAbon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Bn_OK = new System.Windows.Forms.Button();
            this.Bn_Cancel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TB_AE_Commentary = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_AE_Address = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TB_AE_Birthdate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_AE_Secondname = new System.Windows.Forms.TextBox();
            this.TB_AE_Firstname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_AE_Surname = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.DataGrid_AbonLink = new System.Windows.Forms.DataGridView();
            this.TB_LinkNumber = new System.Windows.Forms.TextBox();
            this.Bn_LinkView = new System.Windows.Forms.Button();
            this.Bn_LinkNew = new System.Windows.Forms.Button();
            this.Bn_LinkDelete = new System.Windows.Forms.Button();
            this.Bn_LinkExist = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_AbonLink)).BeginInit();
            this.SuspendLayout();
            // 
            // Bn_OK
            // 
            this.Bn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Bn_OK.Location = new System.Drawing.Point(172, 4);
            this.Bn_OK.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_OK.Name = "Bn_OK";
            this.Bn_OK.Size = new System.Drawing.Size(64, 20);
            this.Bn_OK.TabIndex = 0;
            this.Bn_OK.Text = "Submit";
            this.Bn_OK.UseVisualStyleBackColor = true;
            this.Bn_OK.Click += new System.EventHandler(this.Bn_OK_Click);
            // 
            // Bn_Cancel
            // 
            this.Bn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Bn_Cancel.Location = new System.Drawing.Point(244, 4);
            this.Bn_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Cancel.Name = "Bn_Cancel";
            this.Bn_Cancel.Size = new System.Drawing.Size(64, 20);
            this.Bn_Cancel.TabIndex = 0;
            this.Bn_Cancel.Text = "Cancel";
            this.Bn_Cancel.UseVisualStyleBackColor = true;
            this.Bn_Cancel.Click += new System.EventHandler(this.Bn_Cancel_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Bn_Cancel);
            this.splitContainer1.Panel2.Controls.Add(this.Bn_OK);
            this.splitContainer1.Panel2MinSize = 28;
            this.splitContainer1.Size = new System.Drawing.Size(312, 293);
            this.splitContainer1.SplitterDistance = 264;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(312, 264);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(304, 238);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.TB_AE_Commentary, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.TB_AE_Address, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TB_AE_Birthdate, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.TB_AE_Secondname, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB_AE_Firstname, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.TB_AE_Surname, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(304, 238);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // TB_AE_Commentary
            // 
            this.TB_AE_Commentary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_AE_Commentary.Location = new System.Drawing.Point(88, 161);
            this.TB_AE_Commentary.Margin = new System.Windows.Forms.Padding(4);
            this.TB_AE_Commentary.Multiline = true;
            this.TB_AE_Commentary.Name = "TB_AE_Commentary";
            this.TB_AE_Commentary.Size = new System.Drawing.Size(212, 73);
            this.TB_AE_Commentary.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(4, 165);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 8, 4, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 69);
            this.label6.TabIndex = 19;
            this.label6.Text = "Commentary:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // TB_AE_Address
            // 
            this.TB_AE_Address.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_AE_Address.Location = new System.Drawing.Point(88, 116);
            this.TB_AE_Address.Margin = new System.Windows.Forms.Padding(4);
            this.TB_AE_Address.Multiline = true;
            this.TB_AE_Address.Name = "TB_AE_Address";
            this.TB_AE_Address.Size = new System.Drawing.Size(212, 37);
            this.TB_AE_Address.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(4, 120);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 8, 4, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 33);
            this.label5.TabIndex = 17;
            this.label5.Text = "Address:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // TB_AE_Birthdate
            // 
            this.TB_AE_Birthdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_AE_Birthdate.Location = new System.Drawing.Point(88, 88);
            this.TB_AE_Birthdate.Margin = new System.Windows.Forms.Padding(4);
            this.TB_AE_Birthdate.Name = "TB_AE_Birthdate";
            this.TB_AE_Birthdate.Size = new System.Drawing.Size(212, 20);
            this.TB_AE_Birthdate.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(4, 88);
            this.label4.Margin = new System.Windows.Forms.Padding(4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Birthdate:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_AE_Secondname
            // 
            this.TB_AE_Secondname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_AE_Secondname.Location = new System.Drawing.Point(88, 60);
            this.TB_AE_Secondname.Margin = new System.Windows.Forms.Padding(4);
            this.TB_AE_Secondname.Name = "TB_AE_Secondname";
            this.TB_AE_Secondname.Size = new System.Drawing.Size(212, 20);
            this.TB_AE_Secondname.TabIndex = 14;
            // 
            // TB_AE_Firstname
            // 
            this.TB_AE_Firstname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_AE_Firstname.Location = new System.Drawing.Point(88, 32);
            this.TB_AE_Firstname.Margin = new System.Windows.Forms.Padding(4);
            this.TB_AE_Firstname.Name = "TB_AE_Firstname";
            this.TB_AE_Firstname.Size = new System.Drawing.Size(212, 20);
            this.TB_AE_Firstname.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Second name:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "First name:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Surname:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_AE_Surname
            // 
            this.TB_AE_Surname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_AE_Surname.Location = new System.Drawing.Point(88, 4);
            this.TB_AE_Surname.Margin = new System.Windows.Forms.Padding(4);
            this.TB_AE_Surname.Name = "TB_AE_Surname";
            this.TB_AE_Surname.Size = new System.Drawing.Size(212, 20);
            this.TB_AE_Surname.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(304, 238);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Contacts";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.DataGrid_AbonLink);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.TB_LinkNumber);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkView);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkNew);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkDelete);
            this.splitContainer2.Panel2.Controls.Add(this.Bn_LinkExist);
            this.splitContainer2.Size = new System.Drawing.Size(304, 238);
            this.splitContainer2.SplitterDistance = 210;
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 0;
            // 
            // DataGrid_AbonLink
            // 
            this.DataGrid_AbonLink.AllowUserToAddRows = false;
            this.DataGrid_AbonLink.AllowUserToDeleteRows = false;
            this.DataGrid_AbonLink.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid_AbonLink.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_AbonLink.DefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid_AbonLink.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_AbonLink.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_AbonLink.Margin = new System.Windows.Forms.Padding(0);
            this.DataGrid_AbonLink.MultiSelect = false;
            this.DataGrid_AbonLink.Name = "DataGrid_AbonLink";
            this.DataGrid_AbonLink.ReadOnly = true;
            this.DataGrid_AbonLink.RowHeadersVisible = false;
            this.DataGrid_AbonLink.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.DataGrid_AbonLink.RowTemplate.Height = 20;
            this.DataGrid_AbonLink.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DataGrid_AbonLink.Size = new System.Drawing.Size(304, 210);
            this.DataGrid_AbonLink.TabIndex = 35;
            // 
            // TB_LinkNumber
            // 
            this.TB_LinkNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TB_LinkNumber.Location = new System.Drawing.Point(180, 4);
            this.TB_LinkNumber.Margin = new System.Windows.Forms.Padding(4);
            this.TB_LinkNumber.Name = "TB_LinkNumber";
            this.TB_LinkNumber.Size = new System.Drawing.Size(120, 20);
            this.TB_LinkNumber.TabIndex = 6;
            // 
            // Bn_LinkView
            // 
            this.Bn_LinkView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkView.Location = new System.Drawing.Point(4, 4);
            this.Bn_LinkView.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkView.Name = "Bn_LinkView";
            this.Bn_LinkView.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkView.TabIndex = 2;
            this.Bn_LinkView.Text = "View";
            this.Bn_LinkView.UseVisualStyleBackColor = true;
            this.Bn_LinkView.Click += new System.EventHandler(this.Bn_LinkView_Click);
            // 
            // Bn_LinkNew
            // 
            this.Bn_LinkNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkNew.Location = new System.Drawing.Point(48, 4);
            this.Bn_LinkNew.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkNew.Name = "Bn_LinkNew";
            this.Bn_LinkNew.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkNew.TabIndex = 3;
            this.Bn_LinkNew.Text = "New";
            this.Bn_LinkNew.UseVisualStyleBackColor = true;
            this.Bn_LinkNew.Click += new System.EventHandler(this.Bn_LinkNew_Click);
            // 
            // Bn_LinkDelete
            // 
            this.Bn_LinkDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkDelete.Location = new System.Drawing.Point(136, 4);
            this.Bn_LinkDelete.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkDelete.Name = "Bn_LinkDelete";
            this.Bn_LinkDelete.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkDelete.TabIndex = 4;
            this.Bn_LinkDelete.Text = "Del";
            this.Bn_LinkDelete.UseVisualStyleBackColor = true;
            this.Bn_LinkDelete.Click += new System.EventHandler(this.Bn_LinkDelete_Click);
            // 
            // Bn_LinkExist
            // 
            this.Bn_LinkExist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Bn_LinkExist.Location = new System.Drawing.Point(92, 4);
            this.Bn_LinkExist.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.Bn_LinkExist.Name = "Bn_LinkExist";
            this.Bn_LinkExist.Size = new System.Drawing.Size(40, 20);
            this.Bn_LinkExist.TabIndex = 5;
            this.Bn_LinkExist.Text = "Link";
            this.Bn_LinkExist.UseVisualStyleBackColor = true;
            this.Bn_LinkExist.Click += new System.EventHandler(this.Bn_LinkExist_Click);
            // 
            // FormAbon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 293);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(240, 320);
            this.Name = "FormAbon";
            this.Text = "Abonent";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_AbonLink)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Bn_OK;
        private System.Windows.Forms.Button Bn_Cancel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox TB_AE_Commentary;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_AE_Address;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TB_AE_Birthdate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_AE_Secondname;
        private System.Windows.Forms.TextBox TB_AE_Firstname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_AE_Surname;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView DataGrid_AbonLink;
        private System.Windows.Forms.TextBox TB_LinkNumber;
        private System.Windows.Forms.Button Bn_LinkView;
        private System.Windows.Forms.Button Bn_LinkNew;
        private System.Windows.Forms.Button Bn_LinkDelete;
        private System.Windows.Forms.Button Bn_LinkExist;


    }
}